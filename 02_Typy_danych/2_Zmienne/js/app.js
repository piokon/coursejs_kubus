const a = 2;  // liczba ("number")
const b = "txt";  // string
const c = 2 + "dwa";  // string
const d = true;  // boolean
const e = null;  // object
console.log(a, typeof(a));
console.log(b, typeof(b));
console.log(c, typeof(c));
console.log(d, typeof(d));
console.log(e, typeof(e));
