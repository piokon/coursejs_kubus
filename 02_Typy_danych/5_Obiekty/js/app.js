const car = {
    type: "sedan",
    color: "green",
    engine: 2.0
}
console.log(car.type + " " + car.color + " " + car.engine);

const color = {
    red: 100,
    green: 0,
    blue: 50,
}

const referenceColor = color;
referenceColor.red = 50;
referenceColor.green = 50;

