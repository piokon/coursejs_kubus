function maxFromArray(numbers) {
    return Math.max(...numbers);
}

const randomNumbers = [27, 64, 47, 78, 48, 11, 76, 25, 11, 83];
console.log(maxFromArray(randomNumbers));
