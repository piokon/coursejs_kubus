function indexOfRepeatedValue(array) {
    let firstIndex = null;

    loop0:
        for (let i = 0; i < array.length; i++) {
            for (let j = i+1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    firstIndex = i;
                    break loop0;
                }
            }
        }

    console.log(firstIndex);
    return firstIndex;
}

const numbers = [2, 4, 5, 9, 3, 5, 1, 9, 4];
indexOfRepeatedValue(numbers);
