const n = 3;
const calc = [];
// Piszcie kod pod tym komentarzem.

for (let i = 1; i <= n; i++) {
    calc[i-1] = [];
    for (let j = 1; j <= n; j++) {
        calc[i-1][j-1] = i + " x " + j + " = " + i*j;
    }
}

// print
for (let i = 0; i < calc.length; i++) {
    let rowtxt = "";
    for (let j = 0; j < calc[i].length; j++) {
        if (j > 0) {
            rowtxt += " | ";
        }
        rowtxt += calc[i][j];
  }
  console.log(rowtxt);
}
